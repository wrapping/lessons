'''This module reads 900K+ rows using the list comprehension method'''
import time
import csv


#comprehension_method
BEGIN_COMP_PROCESS = time.time()
FILE_PATH = 'data/data.txt'
with open(FILE_PATH, 'r') as voters:
    DETAIL = [record for record in csv.reader(voters, delimiter='\t')]

END_COMP_PROCESS = time.time()
print('Comprehension Method: {}'.format(END_COMP_PROCESS - BEGIN_COMP_PROCESS))
