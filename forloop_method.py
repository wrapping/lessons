'''This module reads 900K+ rows using the for loop method'''
import time
import csv

#forloop_method
BEGIN_LOOP = time.time()
FILE_PATH = 'data/data.txt'
DATA = []
with open(FILE_PATH, 'r') as voters:
    RECORD_READER = csv.reader(voters, delimiter='\t')
    for row in RECORD_READER:
        DATA.append(row)

END_LOOP = time.time()
print('For Loop Method: {}'.format(END_LOOP - BEGIN_LOOP))
