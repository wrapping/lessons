'''This module reads 900K+ rows using the list() methods'''
import time
import csv

#list_method
BEGIN_LIST_PROCESS = time.time()
FILE_PATH = 'data/data.txt'
with open(FILE_PATH, 'r') as voters:
    LIST_DATA = list(csv.reader(voters, delimiter='\t'))

END_LIST_PROCESS = time.time()
print('List Method: {}'.format(END_LIST_PROCESS - BEGIN_LIST_PROCESS))
